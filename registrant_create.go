package zoom

import "fmt"

// CreateRegistrantOptions are the options to create a registrant for a meeting with
type CreateRegistrantOptions struct {
	MeetingID string `url:"-"`
	Email     string `json:"topic,omitempty"`
	FirstName string `json:"topic,omitempty"`
	LastName  string `json:"topic,omitempty"`
	Phone     string `json:"topic,omitempty"`
}

// CreateRegistrantPath - v2 create a registrant for a meeting
const CreateRegistrantPath = "/meetings/%s/registrants"

// CreateRegistrant calls POST /meetings/{meetingId}/registrants
func CreateRegistrant(opts CreateRegistrantOptions) (Registrant, error) {
	return defaultClient.CreateRegistrant(opts)
}

// CreateRegistrant calls POST /meetings/{meetingId}/registrants

func (c *Client) CreateRegistrant(opts CreateRegistrantOptions) (Registrant, error) {
	var ret = Registrant{}
	return ret, c.requestV2(requestV2Opts{
		Method:         Post,
		Path:           fmt.Sprintf(CreateMeetingPath, opts.MeetingID),
		DataParameters: &opts,
		Ret:            &ret,
	})
}
